# Lancamentos_Baixas_Documentos

Tabela responsável por intermediar o relacionamento entre um lançamento "baixado" e o documento do respectivo lançamento, relacionando, também, o usuário a qual tais dados estão ligados.

## Descrição dos campos da tabela

  | Campo                 | Tipo          | Descrição                                                              |
  | :-------------------- | :------------ | :--------------------------------------------------------------------- |
  | `id`                  | **int8**      | Indentificador da tabela (Primary Key).                                |
  | `lancamento_baixa_id` | **int8**      | Foreing Key da tabela [^^lancamentos_baixas^^](lancamentos-baixas.md). |
  | `documento_id`        | **int8**      | Foreing Key da tabela [^^documentos^^](documentos.md).                 |
  | `user_id`             | **int8**      | Foreing Key da tabela [^^users^^](#) (banco admin).                    |
  | `created_at`          | **timestamp** |                                                                        |
  | `updated_at`          | **timestamp** |                                                                        |

## Relacionamentos

  | Tabela                                            | Tipo            | Descrição                                                        |
  | :------------------------------------------------ | :-------------- | :--------------------------------------------------------------- |
  | [^^`lancamentos_baixas`^^](lancamentos-baixas.md) | **Foreing Key** | Lançamentos que já passaram pelo processo de "baixa" no sistema. |
  | [^^`documentos`^^](documentos.md)                 | **Foreing Key** | Documentos referentes aos lançamentos (anexos).                  |
  | [^^`admin.users`^^](#)                            | **Foreing Key** | Usuário correspondente de um lançamento específico.              |