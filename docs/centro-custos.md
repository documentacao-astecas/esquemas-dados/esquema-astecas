# CentroCustos

Tabela responsável por agrupar as receitas e despesas relacionadas a uma empresa.

## Descrição dos campos da tabela

  | Campo        | Tipo            | Descrição                                          |
  | :----------- | :-------------- | :------------------------------------------------- |
  | `id`         | **int8**        | Indentificador da tabela (Primary Key).            |
  | `empresa_id` | **int8**        | Foreing Key da tabela [^^empresas^^](empresas.md). |
  | `unidade_id` | **int8**        | Foreing Key da tabela [^^unidades^^](unidades.md). |
  | `nome`       | **varchar(40)** | Nome do centro de custos.                          |
  | `descricao`  | **text**        | Descrição do centro de custos.                     |
  | `created_at` | **timestamp**   |                                                    |
  | `updated_at` | **timestamp**   |                                                    |

## Relacionamentos

  | Tabela                                                    | Tipo            | Descrição                                                                  |
  | :-------------------------------------------------------- | :-------------- | :------------------------------------------------------------------------- |
  | [^^`empresas`^^](empresas.md)                             | **Foreing Key** | Uma empresa possui centros de custos, e esses estão ligados a uma empresa. |
  | [^^`unidades`^^](unidades.md)                             | **Foreing Key** | Unidades possuem centros de custos e os mesmos estão relacionados a uma unidade. |
  | [^^`orcamentos_lancamentos`^^](orcamentos-lancamentos.md) | **Primary Key** | Os dados dos centros de custos constarão nos lançamentos dos orçamentos da empresa. |
  | [^^`lancamentos_baixas`^^](lancamentos-baixas.md)         | **Primary Key** | Os dados dos centros de custos estão inseridos nos lançamentos que passarão pelo processo de "baixa". |
  | [^^`lancamentos`^^](lancamentos.md)                       | **Primary Key** | Os dados centros de custos estão contidos em lançamentos.                  |
