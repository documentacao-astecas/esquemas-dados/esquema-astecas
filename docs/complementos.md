# Complementos

Tabela responsável por armazenar informações complementares sobre uma empresa, como por exemplo, diretorias.

## Descrição dos campos da tabela

  | Campo        | Tipo             | Descrição                                                      |
  | :----------- | :--------------- | :------------------------------------------------------------- |
  | `id`         | **int4**         | Indentificador da tabela (Primary Key).                        |
  | `empresa_id` | **int8**         | Foreing Key da tabela [^^empresas^^](empresas.md).             |
  | `unidade_id` | **int4**         | Foreing Key da tabela [^^unidades^^](unidades.md).             |
  | `nome`       | **varchar(255)** | Nome do complemento.                                           |
  | `descricao`  | **text**         | Descrição do complemento.                                      |
  | `status`     | **varchar(1)**   | Identifica se o complemento está "A" (ativo) ou "I" (inativo.) |
  | `created_at` | **timestamp**    |                                                                |
  | `updated_at` | **timestamp**    |                                                                |

## Relacionamentos

  | Tabela                              | Tipo            | Descrição                                                                                    |
  | :---------------------------------- | :-------------- | :------------------------------------------------------------------------------------------- |
  | [^^`empresas`^^](empresas.md)       | **Foreing Key** | Os complementos são relacionados a uma empresa específica.                                   |
  | [^^`unidades`^^](unidades.md)       | **Foreing Key** | Os complementos são relacionados as unidades de uma empresa específica. Informação opcional. |
  | [^^`lancamentos`^^](lancamentos.md) | **Primary Key** | Os diferentes complementos estão contidos dentro das informações dos lançamentos.            |