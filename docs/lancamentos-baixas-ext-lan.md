# Lancamentos_Baixas_Extratos_Lancamentos

Tabela que intermedeia os lançamentos que foram "baixados" e os lançamentos que estão contidos em um extrato.

## Descrição dos campos da tabela

  | Campo                   | Tipo          | Descrição                                                                  |
  | :---------------------- | :------------ | :------------------------------------------------------------------------- |
  | `id`                    | **int8**      | Indentificador da tabela (Primary Key).                                    |
  | `lancamento_baixa_id`   | **int8**      | Foreing Key da tabela [^^lancamentos_baixas^^](lancamentos-baixas.md).     |
  | `extrato_lancamento_id` | **int8**      | Foreing Key da tabela [^^extratos_lancamentos^^](extratos-lancamentos.md). |
  | `user_id`               | **int8**      | Foreing Key da tabela [^^users^^](#) (banco admin).                        |
  | `created_at`            | **timestamp** |                                                                            |
  | `updated_at`            | **timestamp** |                                                                            |

## Relacionamentos

  | Tabela                                                | Tipo            | Descrição                                                  |
  | :---------------------------------------------------- | :-------------- | :--------------------------------------------------------- |
  | [^^`lancamentos_baixas`^^](lancamentos-baixas.md)     | **Foreing Key** | Lançamentos que já foram "baixados".                       |
  | [^^`extratos_lancamentos`^^](extratos-lancamentos.md) | **Foreing Key** | Lançamentos com um dado status contidos em um extrato.     |
  | [^^`admin.users`^^](#)                                | **Foreing Key** | Os dados dos usuários a qual os lançamentos correspondem.  |