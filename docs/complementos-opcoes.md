# Complementos_Opcoes

Agrupa sub-complementos armazenados na tabela de complementos. Por exemplo, uma diretoria pode ser subdividida em diretoria executiva.

## Descrição dos campos da tabela

  | Campo            | Tipo             | Descrição                                                      |
  | :--------------- | :--------------- | :------------------------------------------------------------- |
  | `id`             | **int8**         | Indentificador da tabela (Primary Key).                        |
  | `complemento_id` | **int4**         | Foreing Key da tabela [^^complementos^^](complementos.md).     |
  | `nome`           | **varchar(255)** | Nome do sub-complemento.                                       |
  | `descricao`      | **text**         | Descrição do sub-complemento.                                  |
  | `status`         | **varchar(1)**   | Identifica se o complemento está "A" (ativo) ou "I" (inativo.) |
  | `created_at`     | **timestamp**    |                                                                |
  | `updated_at`     | **timestamp**    |                                                                |

## Relacionamentos

  | Tabela                                | Tipo            | Descrição                                                                |
  | :------------------------------------ | :-------------- | :----------------------------------------------------------------------- |
  | [^^`complementos`^^](complementos.md) | **Primary Key** | As opções de complementos são ramificações de um complemento mais geral. |
